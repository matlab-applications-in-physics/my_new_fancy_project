% This is .m script file
% Let's display today's date
% 
% author: Me
% date: 2019-11-07
% version: 0.2 alpha2
%

whatTimeIsIt = clock;

disp(['Dziś mamy: ',... 
    num2str(whatTimeIsIt(1)), '-',...
    num2str(whatTimeIsIt(2)), '-',...
    num2str(whatTimeIsIt(3))])
disp(['godzina: ', num2str(whatTimeIsIt(4)), ':', num2str(whatTimeIsIt(5))]) 