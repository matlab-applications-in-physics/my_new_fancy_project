% Matlab R2020b script
% name: moj_skrypt.m
% author: Jacek Pawlyta
% date: 2020-11-13
% version: 0.1

%part 1 
disp('part 1')
r = 2; % r is in m
area = pi*r^2; % area is in m²
disp('r = 2; area = pi*r^2;') 
disp(['pole powierzchni koła o promieniu ',num2str(r),' m wynosi ',num2str(area),' m²'])

%part 2
disp('part 2')
r = 2; % r is in m
volume = 4/3*pi*r^3; % volume is in m³
disp('r = 2; volume = 4/3*pi*r^3') 
disp(['objętość kuli o promieniu ',num2str(r),' m wynosi ',num2str(volume),' m³'])